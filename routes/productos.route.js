const { Router } = require('express');
const { check } = require('express-validator');

const { validarCampos, validarJWT, tieneRol } = require('../middlewares/index.middlewares');

const { existeCategoria, existeProducto } = require('../helpers/db-validators');

const {
    obtenerProductos,
    crearProducto,
    obtenerProducto,
    actualizarProducto,
    eliminarProducto} = require('../controllers/productos.controller');

const router = Router();

router.get('/', obtenerProductos);

router.get('/:id',[
    check('id','Debe ser un id de Mongo').isMongoId(),
    check('id').custom(existeProducto),
    validarCampos
],obtenerProducto);


router.post('/',[
    validarJWT,
    check('nombre','El nombre del producto es obligatorio').notEmpty(),
    check('categoria','No es un id de Mongo').isMongoId(),
    check('categoria').custom(existeCategoria),
    validarCampos
],crearProducto);

router.put('/:id',[
    validarJWT,
    check('categoria','No es un id de Mongo').isMongoId(),
    check('id','Debe ser un id de Mongo'),
    check('id').custom(existeProducto),
    validarCampos
],actualizarProducto);

router.delete('/:id',[
    validarJWT,
    tieneRol('ADMIN_ROLE'),
    check('id','Debe ser un id de Mongo'),
    check('id').custom(existeProducto),
    validarCampos
],eliminarProducto);

module.exports = router;