const { Router } = require('express');
const { check } = require('express-validator');

const { validarCampos, validarArchivos } = require('../middlewares/index.middlewares');
const { coleccionesPermitidas } = require('../helpers/index.helpers');

const { cargarArchivo, actualizarImagen, mostrarImagen } = require('../controllers/uploads.controller');



const router = Router();

router.get('/:coleccion/:id',[
    check('id','Debe de ser un id de MongoDB').isMongoId(),
    check('coleccion').custom( c => coleccionesPermitidas( c, [ 'usuarios', 'productos' ] ) ),
    validarCampos
],mostrarImagen);

router.post('/',[
    validarArchivos,
    validarCampos
],cargarArchivo);

router.put('/:coleccion/:id',[
    validarArchivos,
    check('id','Debe de ser un id de MongoDB').isMongoId(),
    check('coleccion').custom( c => coleccionesPermitidas( c, [ 'usuarios', 'productos' ] ) ),
    validarCampos
],actualizarImagen);

module.exports = router;