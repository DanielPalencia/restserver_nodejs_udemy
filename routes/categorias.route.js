const { Router } = require('express');
const {check} = require('express-validator');

const {
    crearCategoria,
    obtenerCategorias,
    obtenerCategoria,
    actualizarCategoria,
    eliminarCategoria   } = require('../controllers/categoria.controller');

const { validarJWT, validarCampos, tieneRol } = require('../middlewares/index.middlewares');

const {existeCategoria} = require('../helpers/db-validators');

const router = Router();


// Obtener todas las categorias
router.get('/', obtenerCategorias);

// Obtener una categoria
router.get('/:id',[
    check('id', 'Debe de ser un id de Mongo').isMongoId(),
    check('id').custom(existeCategoria),
    validarCampos,
],obtenerCategoria);

// Crear nueva categoria - privado
router.post('/',[
    validarJWT,
    check('nombre', 'El nombre es obligatorio').notEmpty(),
    check('estado','El estado es obligatorio').notEmpty(),
    validarCampos
],crearCategoria);

// Actualizar categoria - privado
router.put('/:id',[
    validarJWT,
    tieneRol('ADMIN_ROLE'),
    check('nombre','El campo "nombre" debe de estar lleno').notEmpty(),
    check('id','El campo "id" debe de ser del tipo ObjetctID de MongoDB').isMongoId(),
    check('id','El campo "id" debe de existir').custom(existeCategoria),
    validarCampos
],actualizarCategoria);

// Deshabilitar categoria - Admin
router.delete('/:id',[
    validarJWT,
    tieneRol('ADMIN_ROLE'),
    check('id','El campo "id" debe de ser del tipo ObjetctID de MongoDB').isMongoId(),
    check('id').custom(existeCategoria),
    validarCampos
],eliminarCategoria);


module.exports = router;