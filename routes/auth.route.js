const { Router } = require('express');
const { check } = require('express-validator');

const { login, googleSignin } = require('../controllers/auth.controller');

const { validarCampos } = require('../middlewares/validar-campos');

const router = Router();

router.post('/login',[
    check('correo','El correo es obligatorio').notEmpty(),
    check('correo','El campo introducido no es un correo').isEmail(),
    check('password','La password es obligatoria').notEmpty(),
    validarCampos
], login);

router.post('/google',[
    check('id_token','El id token es necesario').notEmpty(),
    validarCampos
],googleSignin);

module.exports = router;