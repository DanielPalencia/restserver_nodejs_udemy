const { Router } = require('express');
const {check} = require('express-validator');

const {esRoleValido, esEmailRegistrado, existeUsuarioID} = require('../helpers/db-validators');

const {
    validarCampos,
    validarJWT,
    tieneRol,
} = require('../middlewares/index.middlewares');

const {
    usuariosGet,
    usuariosPost,
    usuariosDelete,
    usuariosPut } = require('../controllers/usuarios.controller');



const router = Router();

router.get('/', usuariosGet);

router.put('/:id',[
    check('id','No es un ID valido').isMongoId(),
    check('id', 'No existe el usuario').custom(existeUsuarioID),
    check('rol').custom(esRoleValido),
    validarCampos,
],usuariosPut);

router.post('/', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'El password debe de contener almenoss 4 caracteres.').isLength({min:4}),
    check('correo', 'El correo no es válido').isEmail(),
    check('correo','El correo ya existe').custom(esEmailRegistrado),
    // check('rol', 'No es un rol permitido').isIn(['ADMIN_ROLE','USER_ROLE']),
    check('rol').custom(esRoleValido),
    validarCampos
],usuariosPost);

router.delete('/:id',[
    validarJWT,
    tieneRol('ADMIN_ROLE'),
    check('id','No es un ID valido').isMongoId(),
    check('id', 'No existe el usuario').custom(existeUsuarioID),
    validarCampos
],usuariosDelete);

module.exports = router;