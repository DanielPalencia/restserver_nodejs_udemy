const mongoose = require('mongoose');


const dbConnection = async() =>{
    try {
        await mongoose.connect(process.env.MONGODB_CNN,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify:false
        });

        console.log('conexion a la DB realizada');
    } catch (error) {
        console.log(`Causa:\n${error}\n`);
        throw new Error('Error a la hora de conectar a la DB: cafe_udemy');
    }
}

module.exports = {
    dbConnection
}