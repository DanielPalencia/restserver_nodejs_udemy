const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const { dbConnection } = require('../database/config.db');

class Server{
    constructor(){
        this.app = express();
        this.port = process.env.PORT;

        this.path = {
            auth:       '/api/auth',
            buscar:     '/api/buscar',
            categorias: '/api/categorias',
            usuarios:   '/api/usuarios',
            uploads: '/api/uploads',
            productos: '/api/productos',
        };

        // Conectar a DB
        this.conectarDB();

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicacion
        this.routes();
    }

    async conectarDB(){
        await dbConnection();
    }

    middlewares(){
        //CORS
        this.cors = cors();

        // Lectura y parseo del body
        this.app.use( express.json() );

        // Directorio publico
        this.app.use( express.static('public') );

        // FileUpload - Carga de archivos
        this.app.use(fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/',
            createParentPath : true,
        }));
    }

    routes(){
        this.app.use(this.path.auth,require('../routes/auth.route'));
        this.app.use(this.path.buscar,require('../routes/buscar.route'));
        this.app.use(this.path.categorias,require('../routes/categorias.route'));
        this.app.use(this.path.usuarios,require('../routes/usuarios.route'));
        this.app.use(this.path.uploads,require('../routes/uploads.route'));
        this.app.use(this.path.productos,require('../routes/productos.route'));
    }

    listen(){
        this.app.listen(this.port, ()=>{
            console.log(`Example app listening at http://localhost:${this.port}`);
        });

    }
}

module.exports = Server;