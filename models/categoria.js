const { model, Schema } = require('mongoose');

const CategoriaSchema = Schema({
    nombre:{
        type: String,
        required:[true,'El nombre de la categoria es obligatorio'],
        unique: true
    },
    estado:{
        type: Boolean,
        default:true,
        required:[true,'El estado de la categoria es obligatoria']
    },
    usuario:{
        type: Schema.Types.ObjectId,
        ref: 'Usuarios',
        require: [true,'Es obligatorio especificar el usuario que lo ha creado']
    }

});

CategoriaSchema.methods.toJSON = function(){
    const { __v, ...categoria } = this.toObject();
    return categoria;
}


module.exports = model('Categoria',CategoriaSchema);