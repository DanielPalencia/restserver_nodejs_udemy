const { model, Schema } = require('mongoose');

const ProductoSchema = Schema({
    nombre:{
        type: String,
        required:[true,'El nombre es obligatorio']
    },
    estado:{
        type:Boolean,
        required:[true,'Introduce el estado del producto'],
        default:true
    },
    precio:{
        type: Number,
        default: 0
    },
    descripcion:{ type:String },
    disponible: { type:Boolean, default: true },
    categoria:{
        type: Schema.Types.ObjectId,
        ref: 'Categoria',
        required:[true,'Debes añadir el producto a una categoria']
    },
    usuario:{
        type: Schema.Types.ObjectId,
        ref: 'Usuarios',
        required:[true,'Introduce el id del usuario que quiere crear el producto']
    },
    img:{
        type: String
    },

});

ProductoSchema.methods.toJSON = function(){
    const { __v, ...producto } = this.toObject();
    return producto;
}

module.exports = model('Producto',ProductoSchema);