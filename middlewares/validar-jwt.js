const { request, response } = require('express');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario');

const validarJWT = async(req = request, res = response, next) =>{

    try {
        const token = req.header('x-token');

        if (!token){
            return res.status(401).json({
                msg: 'no hay token en la peticion'
            })
        }

        const {uid} = jwt.verify(token,process.env.SECRETKEY);

         const { __v, password, ...usuario } = await Usuario.findById(uid).lean();

        // Verifica si el usuario existe
        if (!usuario) {
            return res.json({
                msg: 'Token no valido - no existe el usuario'
            })
        }

        // Verificar si el estado de usuario es activo
        if (!usuario.estado) {
               return res.json({
                   msg: 'Token no valido - usuario inactivo'
               })
        }

        req.usuario = usuario;

        next();

    } catch (error) {
        console.log(error);
        res.status(401).json({
            succes: false,
            msg:'token no valido'
        })
    }

}

module.exports = {
    validarJWT,
}