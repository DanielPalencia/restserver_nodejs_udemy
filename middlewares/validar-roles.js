const {response} = require('express');

const tieneRol = (...roles) =>{
    try {
        return (req = request, res = response, next) => {
            const {rol, nombre} = req.usuario

            if (!req.usuario) {
                return res.status(500).json({
                    msg: 'Se quiere verificar el rol sin validar el token primero'
                })
            }

            if (!roles.includes(rol)) {
                return res.status(401).json({
                    msg: `${nombre} no tiene los permisos necesarios`
                })
            }

            next()
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error al comprobar los roles del usuario contacte con el administrador'
        })
    }
}

module.exports = {
    tieneRol,
}