const { Producto, Categoria, Usuario, Role } = require('../models/index.model');

const esRoleValido =  async(rol = '') =>{
    const existeRol = await Role.findOne( {rol} );

    if(!existeRol){
        throw new Error(`El rol ${rol} no esta registrado en la DB`);
    }

    return true;
}

const esEmailRegistrado = async(correo = '') =>{
    const existeEmail = await Usuario.findOne({correo});
    if(existeEmail){
        throw new Error(`El correo ${correo} ya esta registrado`);
    }

    return true;
}

const existeUsuarioID = async(id='') =>{
    const existeUsuario =  await Usuario.findById(id);
    if (!existeUsuario) {
        throw new Error(`El id ${id} no existe`);
    }

    return true;
}

const existeCategoria = async(id='') =>{
    const existeCategoria = await Categoria.findById(id);

    if (!existeCategoria) {
        throw new Error(`El id: ${id} no pertenece a ninguna categoria existente'`);
    }

    return true;
}

const existeProducto = async(id='') =>{
    const existeProducto = await Producto.findById(id);

    if (!existeProducto) {
        throw new Error(`El id: ${id} no pertenece a ningun producto existente'`);
    }

    return true;
}

const coleccionesPermitidas = (coleccion = '', colecciones = []) =>{
    const estaIncluida = colecciones.includes(coleccion);

    if(!estaIncluida){
        throw new Error(`La coleccion ${coleccion} no es permitida, ${colecciones}`);
    }

    return true;

}

module.exports = {
    esRoleValido,
    esEmailRegistrado,
    existeUsuarioID,
    existeCategoria,
    existeProducto,
    coleccionesPermitidas,
}