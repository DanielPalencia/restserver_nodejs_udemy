const jwt = require('jsonwebtoken');

const generarJWT = (uid = '') =>{

    return new Promise( (resolve,reject) =>{
        const payload = { uid }

        jwt.sign(payload ,process.env.SECRETKEY,{
            expiresIn:'7d'
        },(err, token) =>{

            if(err){
                reject('No se a podido generar el token');
            }else{
                resolve(token);
            }

        })

    } )
}

module.exports = {
    generarJWT
}