const path = require('path');
const { v4: uuidv4 } = require('uuid');

const DefaultExtensions =  ['png','jpg','gif'];

const subirArchivo = ( files, carpeta = '', extensionesValidas = DefaultExtensions ) =>{

    return new Promise( (resolve, reject) =>{
        // Obtenemos el archivo
        const{ archivo } = files;

        const nombreCortado = archivo.name.split('.');
        const extension = nombreCortado[ nombreCortado.length - 1 ];

        // Valida las extensiones
        if (!extensionesValidas.includes(extension)) {
            return reject({
                msg:`La extension ${extension} no es valida`,
                extensionesValidas
            });
        }

        const nombTemp = uuidv4() + '.' + extension; // Genera un identificador unico

        // Definimos el nombre del archivo y la ruta donde se guardara
        const uploadPath = path.join( __dirname, '../uploads/', carpeta, nombTemp );

        // Movemos el archivo a la ruta definida en uploadPath
        archivo.mv(uploadPath, function(err) {
            if (err){
                return reject (err);
            }

            resolve(nombTemp);
        });
    });

}


module.exports = {
    subirArchivo,
}