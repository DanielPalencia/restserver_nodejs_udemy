const { response, request } = require('express');
const bcryptjs = require('bcryptjs');

const Usuario = require('../models/usuario');


const usuariosGet = async(req = request, res = response) =>{
    try {
        const { limite = 5, desde = 0 } = req.query;
        const query =  {estado:true};

        const [total, usuarios] = await Promise.all([
            Usuario.countDocuments(query),
            Usuario.find(query)
                .skip(Number(desde))
                .limit(Number(limite))
        ]);

        res.json({
            succes: true,
            total,
            usuarios
        });

    } catch (error) {
        console.error(error);
        throw new Error('Error al obtener los usuarios');
    }

}

const usuariosPut = async(req, res = response) =>{

    try {
        const { id } = req.params;
        const {password, google, ...resto} = req.body;

        // TODO validar contra DB
        if( password ){
        // Encriptar la contraseña
            const salt = bcryptjs.genSaltSync();
            resto.password = bcryptjs.hashSync(password, salt);
        }

        const usuario = await Usuario.findByIdAndUpdate(id, resto);

        res.json({
            usuario
        });
    } catch (error) {
        console.error({error});
        res.json({
            succes: false,
            msg: "Error on update user"
        });
    }
}

const usuariosPost = async (req, res = response) =>{
    try {
        const { nombre, password, correo, rol } = req.body;
        const usuario = new Usuario({ nombre, correo, password, rol });

        // Encriptar la contraseña
        const salt = bcryptjs.genSaltSync();
        usuario.password = bcryptjs.hashSync(password,salt);

        // Guardar en BD
        await usuario.save();

        res.json({
            usuario
        });
    } catch (error) {
        console.error({error});
        res.json({
            succes: false,
            msg: 'Error in data base'
        });
    };
}

const usuariosDelete = async(req = request, res = response) =>{
    const {id} = req.params;

    // Fisicamente lo borramos
    //const usuario = await Usuario.findByIdAndDelete(id);

    const usuario = await Usuario.findByIdAndUpdate(id, {estado: false});
    const usuarioAutenticado = req.usuario;

    res.json({
        succes: true,
        usuario,
        usuarioAutenticado
    });
}

const usuariosPatch = (req, res = response) =>{
    res.json({
        succes: true,
        msg: "patch API - controlador"
    });
}

module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosPut,
    usuariosDelete,
    usuariosPatch
}