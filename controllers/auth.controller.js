const { response } = require('express');
const bcryptjs = require('bcryptjs');

const Usuario = require('../models/usuario');

const { generarJWT } = require('../helpers/generar-jwt');
const { googleVerify } = require('../helpers/google-veryfy');

const login = async(req,res = response) =>{
    try {
        const { correo, password:passwd} = req.body;

        // Verificar si el email existe
        const usuario = await Usuario.findOne({correo})

        if (!usuario){
            return res.status(400).json({
                succes:"error",
                msg:'El correo introducido no existe en el sistema'
            })
        }

        // Verifica si el usuario esta activo
        if (!usuario.estado){
            return res.status(400).json({
                succes:"error",
                msg:'El usuario esta deshabilitado'
            })
        }

        // Verificar la contraseña
        const validPassword = bcryptjs.compareSync(passwd, usuario.password);

        if (!validPassword) {
            return res.status(400).json({
                succes:"error",
                msg:'usuario o password incorrecto - password'
            })
        }

        // Generar el JWT
        const token =  await generarJWT(usuario.id);

        res.json({
            usuario,
            token
        })

    } catch (error) {
        console.log(error);
        return   res.json({
            succes: "false",
            msg: "Algo salio mal... Pongase en contacto con el administrador"
        })
    }


}

const googleSignin = async(req = request, res = response) =>{
    try {
        const {id_token} = req.body;

        const {correo,nombre,img} = await googleVerify(id_token);

        let usuario = await Usuario.findOne({correo});
        if (!usuario) {
            // Creamos usuario
            const data = {
                nombre,
                correo,
                password: ':P',
                img,
                google: true
            };

            usuario = new Usuario(data);
            usuario.save();
        }

        // Comprobamos si el usuario esta habilitado
        if ( !usuario.estado ) {
            return res.status(401).json({
                msg:'Hable con el administrador, usuario bloqueado'
            });
        }

        const token = await generarJWT(usuario.id);

        res.json({
            msg: 'Google sign-in ok',
            usuario,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            msg:'Token de google no reconocido'
        })
    }
}

module.exports = {
    login,
    googleSignin
}