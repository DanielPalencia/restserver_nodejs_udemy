const { response, request } = require('express');
const { body } = require('express-validator');
const {Producto} = require('../models/index.model');
;


// Obtener Productos - paginado - total
const obtenerProductos =  async(req = request, res = response) =>{
    try {
        const { limite = 5, desde = 0 } = req.query;
        const query =  {estado:true};

        const [total, productos] = await Promise.all([
            Producto.countDocuments(query),
            Producto.find(query)
                .populate('usuario','nombre')
                .populate('categoria','nombre')
                .skip(Number(desde))
                .limit(Number(limite))
        ]);

        res.json({
            succes: true,
            total,
            productos
        });

    } catch (error) {
        console.error(error);
        throw new Error('Algo a impedido obtener los productos, pongase en contacto con el administrador');
    }
}

// Obtener Producto
const obtenerProducto =  async(req = request, res = response) =>{
    try {
        const {id} = req.params;

        const producto = await Producto.findById(id)
            .populate('usuario','nombre')
            .populate('categoria','nombre')

        if(!producto){
            return res.status(404).json({msg:`No existe ninguna producto con el id:${id}`});
        }

        res.status(200).json({
            succes: true,
            producto
        });

    } catch (error) {
        console.log(error);
        return res.status(501).json({
            succes: false,
            msg: 'Algo a impedido obtener el producto, pongase en contacto con el administrador'
        });
    }
}


// Crear Producto - autentificacion - permisos - añadirCategoria - añadirUsario
const crearProducto =  async(req = request, res = response) =>{
    try {
        const {
            nombre,
            estado,
            usuario,
            ...body } = req.body;

        const productoDB = await Producto.findOne( { nombre:nombre.toUpperCase() } );

        // Comprueba si ya existe el producto
        if(productoDB){
            return res.json(`Ya existe un producto con el nombre: ${productoDB.nombre}`);
        }

        // Generar la data a guardar
        const data = {
            ...body,
            nombre:nombre.toUpperCase(),
            usuario: req.usuario._id,
        }

        // Creamos el objeto y lo guardamos en la DB
        const producto = new Producto(data);
        await producto.save();

        res.status(201).json(producto);
    } catch (error) {
        console.log(error);
        return res.status(501).json('Algo a impedido crear el producto, pongase en contacto con el administrador');
    }
}

// Actualizar Producto - autentificacion - permisos - existeProducto - existe categoria - añadirUsario
const actualizarProducto =  async(req = request, res = response) =>{
    try {
        const {id} = req.params;
        const {estado, usuario, ...data} = req.body;

        if(data.nombre){
            data.nombre = data.nombre.toUpperCase();
        }

        data.usuario = req.usuario._id;

        const producto = await Producto.findByIdAndUpdate(id,data,{new:true}).populate('usuario','nombre').populate('categoria','nombre');

        res.status(200).json({
            producto
        });
    } catch (error) {
        console.log(error);
        return res.status(501).json({
            succes:false,
            msg:'Error inesperado al actualizar el producto, contacte con el administrador'
        })
    }
}

// Eliminar Producto - autentificacion - permisos - existeProducto - añadirUsario
const eliminarProducto =  async(req = request, res = response) =>{
    try {
        const {id} = req.params;
        const productoBorrado = await Producto.findByIdAndUpdate( id,{ estado:false },{ new:true });

        res.status(200).json({
            msg: `Se ha modificado el estado del producto correctamente`,
            productoBorrado
        });
    } catch (error) {
        console.log(error);
        return res.status(501).json('Error inesperado al intentar eliminar el producto, contacte con el administrador');
    }
}


module.exports = {
    obtenerProductos,
    crearProducto,
    obtenerProducto,
    actualizarProducto,
    eliminarProducto
}