const path = require('path');
const fs = require('fs');

const { response, request } = require("express");
const { subirArchivo } = require("../helpers/subir-archivo");

const {Usuario, Producto} = require('../models/index.model');

const cargarArchivo = async(req = request, res = response) =>{
    try {
        // Imagenes
        const nombre = await subirArchivo( req.files, 'imgs', undefined);

        return res.json( { nombre } );

    } catch (msg) {
        return res.status(500).json( { msg } );
    }

}

const actualizarImagen = async(req = request ,res = response) =>{
    const { id, coleccion } = req.params;

    let modelo;

    switch (coleccion) {
        case 'usuarios':
                modelo = await Usuario.findById(id);

                if (!modelo) {
                    return res.status(400).json({
                        msg: `No existe ningun usuario con el id ${id}`
                    })
                }
            break;
        case 'productos':
            modelo = await Producto.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    msg: `No existe ningun producto con el id ${id}`
                })
            }

            break;

        default:
            return res.status(500).json( { msg: `No se puede actualizar la imagen de ${coleccion}, pongase en contacto con el administrador`} );
    }

    // Limpiar imagenes previas
    if (modelo.img) {
        // hay que borrar la imagen del servidor
        const pathImagen = path.join( __dirname, '../uploads', coleccion, modelo.img  );

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
            console.log('borra imagen');
        }
    }

    const nombre = await subirArchivo(req.files, coleccion, undefined );
    modelo.img = nombre
    await modelo.save();

    res.json({
        modelo
    });
}

const mostrarImagen = async(req, res = response) =>{
    const { id, coleccion } = req.params;

    let modelo;

    switch (coleccion) {
        case 'usuarios':
                modelo = await Usuario.findById(id);

                if (!modelo) {
                    return res.status(400).json({
                        msg: `No existe ningun usuario con el id ${id}`
                    })
                }
            break;
        case 'productos':
            modelo = await Producto.findById(id);

            if (!modelo) {
                return res.status(400).json({
                    msg: `No existe ningun producto con el id ${id}`
                })
            }

            break;

        default:
            return res.status(500).json( { msg: `No se puede actualizar la imagen de ${coleccion}, pongase en contacto con el administrador`} );
    }

    let pathImagen;

    if (modelo.img) {
        pathImagen = path.join( __dirname, '../uploads', coleccion, modelo.img  );

        if (fs.existsSync(pathImagen)) {
            return res.sendFile(pathImagen);
        }
    }

     pathImagen = path.join( __dirname, '../assets/no-image.png' );

     return res.sendFile(pathImagen);

}

module.exports = {
    cargarArchivo,
    actualizarImagen,
    mostrarImagen,
}