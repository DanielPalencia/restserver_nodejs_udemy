const { response, request } = require('express');

const Categoria = require('../models/categoria');

const obtenerCategorias = async(req = request, res = response) => {
    try {
        const {desde = 0, limite = 1} = req.query;
        const estado =  true;

        const [total, categorias] = await Promise.all([
            await Categoria.find({estado}).countDocuments(),
            await Categoria.find({estado})
                .populate('usuario','nombre')
                .skip(Number(desde))
                .limit(Number(limite))
        ]);


        res.status(200).json({
            succes: true,
            total,
            categorias
        });

    } catch (error) {
        console.log(error);
        return res.status(501).json({
            succes: false,
            msg: 'Algo a impedido obtener las categorias, pongase en contacto con el administrador'
        });
    }

}

const obtenerCategoria = async(req = request, res = response) => {
    try {
        const {id} = req.params;
        // const estado =  true;

        const categoria = await Categoria.findById(id)
            .populate('usuario','nombre');

        if(!categoria){
            return res.status(404).json({msg:`No existe ninguna categoria con el id:${id}`});
        }

        res.status(200).json({
            succes: true,
            categoria
        });

    } catch (error) {
        console.log(error);
        return res.status(501).json({
            succes: false,
            msg: 'Algo a impedido obtener las categorias, pongase en contacto con el administrador'
        });
    }
}

const crearCategoria = async(req = request, res = response) => {
    try {
        const nombre = req.body.nombre.toUpperCase();

        const categoriaDB = await Categoria.findOne({nombre});

        // Comprueba si ya existe la categoria
        if(categoriaDB){
            return res.json(`La categoria ${categoriaDB.nombre} ya existe`);
        }

        // Generar la data a guardar
        const data = {
            nombre,
            usuario: req.usuario._id,
        }

        // Creamos el objeto y lo guardamos en la DB
        const categoria = new Categoria(data);
        await categoria.save();

        res.status(201).json(categoria);
    } catch (error) {
        console.log(error);
        return res.status(501).json('Algo a impedido crear la categoria, pongase en contacto con el administrador');
    }
}


// func actualizar categoria
const actualizarCategoria = async(req = request, res = response) => {
    try {
        const {id} = req.params;
        const {estado, usuario, ...data} = req.body;

        data.nombre = data.nombre.toUpperCase();
        data.usuario = req.usuario._id;

        const categoria = await Categoria.findByIdAndUpdate(id,data,{new:true}).populate('usuario','nombre');

        res.status(200).json({
            categoria
        });
    } catch (error) {
        console.log(error);
        return res.status(501).json({
            succes:false,
            msg:'Error inesperado al actualizar la categoria, contacte con el administrador'
        })
    }
}

// func borrar categoria
const eliminarCategoria = async(req = request, res = response) =>{
    try {
        const {id} = req.params;
        const categoriaBorrada = await Categoria.findByIdAndUpdate( id,{ estado:false },{ new:true });

        res.status(200).json({
            msg: 'Se ha modificado el estado de la categoria correctamente',
            categoriaBorrada
        });
    } catch (error) {
        console.log(error);
        return res.status(501).json('Error inesperado al intentar eliminar la categoria, contacte con el administrador');
    }
}

module.exports = {
    crearCategoria,
    obtenerCategorias,
    obtenerCategoria,
    actualizarCategoria,
    eliminarCategoria
}