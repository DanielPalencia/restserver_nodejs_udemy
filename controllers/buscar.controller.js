const {response, request} = require('express');
const { isValidObjectId } = require('mongoose');

const { Usuario, Categoria, Producto } = require('../models/index.model');

const coleccionesPermitidas = [
    'categorias',
    'productos',
    'roles',
    'usuarios'
];

const tiposPermitidos = [
    'categorias',
];


const buscarProductoEnCategoria = async(categoria = '', producto = '', res = response) =>{
    try {
        const objCategoria  = await Categoria.findOne({ nombre : categoria.toUpperCase() } );

        if (!objCategoria) {
            return res.json(`No existe la categoria ${categoria}`);
        }

        const { _id:categoriaID } = objCategoria;

        const regex = RegExp(producto,'i');

        productos = await Producto.find({
            nombre:regex,
            estado:true,
            categoria:{
                _id : categoriaID
            }
        })

        return res.json(productos);

    } catch (error) {
        console.log(error);
        return res.status(500).json('Algo ha impedido realizar la busqueda del producto por categoria, pongase en contacto con el administrador');
    }
}

const buscarCategorias = async(termino = '', res = response) => {
    try {
        const esMongoID = isValidObjectId(termino);

        if (esMongoID) {
            const categoria = await Categoria.findById(termino);

            return res.json({
                results: categoria ?  [categoria] : []
            });
        }

        const regex = new RegExp(termino,'i');

        const categorias = await Categoria.find( { nombre:regex, estado:true } );

        return res.json({
            total: categorias.length,
            results: categorias ? [categorias] : [],
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json('Algo ha impedido realizar la busqueda por categoria, pongase en contacto con el administrador');
    }
}

const buscarProductos = async(termino = '', res = response) => {
    try {
        const esMongoID = isValidObjectId(termino);

        if (esMongoID) {
            const producto = await  Producto.findById(termino)
                .populate('categoria','nombre')
                .populate('usuario','nombre')

            return res.json({
                results: producto ?  [producto] : []
            });
        }

        const regex = new RegExp(termino,'i');

        const productos = await Producto.find( { nombre:regex, estado:true } )
            .populate('categoria','nombre')
            .populate('usuario','nombre')


        return res.json({
            total: productos.length,
            results: productos ? [productos] : [],
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json('Algo ha impedido realizar la busqueda por producto, pongase en contacto con el administrador');
    }
}

const buscarUsuarios = async(termino= '', res = response) =>{
    const esMongoID =  isValidObjectId(termino);

    if (esMongoID) {
        const usuario = await Usuario.findById(termino);

        return res.json({
            results: usuario ? [usuario] : []
        })
    }

    const regex = new RegExp(termino,'i');

    const usuarios = await Usuario.find({
        $or: [ { nombre: regex }, { correo:regex }],
        $and: [ { estado:true } ]
    });

    return res.json({
        total: usuarios.length,
        results: usuarios
    });

}


const buscar = (req = request, res = response) =>{

    const { tipo, coleccion , termino}  = req.params;

    if (tipo) {

        if (!tiposPermitidos.includes(tipo)) {
            return res.status(400).json({
                msg: `El tipo: ${tipo} no esta incluido, tipos permitidos: ${ coleccionesPermitidas }`
            })
        }

        switch (tipo) {
            case tiposPermitidos[0]:
                return buscarProductoEnCategoria(coleccion,termino,res);
                default:
                    return res.status(500).json({
                        msg: `Error al realizar la busqueda en tipo: ${ tipo }, contacte con el administrador`,
                    })
        }
    }

    if (!coleccionesPermitidas.includes(coleccion)) {
        return res.status(400).json({
            msg: `La coleccion: ${coleccion} no esta incluida, colecciones permitidas: ${ coleccionesPermitidas }`
        })
    }


    switch (coleccion) {
        case 'categorias':
            buscarCategorias(termino,res);
            break;
        case 'productos':
            buscarProductos(termino,res);
            break;
        case 'usuarios':
            buscarUsuarios(termino, res);
            break;
        default:
            return res.status(500).json({
                msg: `Error al realizar la busqueda en la coleccion: ${ coleccion }, contacte con el administrador`,
            })
    }
}

module.exports = {
    buscar,
}